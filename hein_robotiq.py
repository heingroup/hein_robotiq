from typing import Union, List
import time
import minimalmodbus


class HandEGripper:
    AUTORELEASE_OPENING = 'opening'
    AUTORELEASE_CLOSING = 'closing'

    STATUS_REGISTER = 2000
    FAULT_AND_POSITION_REGISTER = 2001
    POSITION_AND_CURRENT_REGISTER = 2002
    ACTION_REGISTER = 1000

    def __init__(self, com_port: Union[str, int], address: int = 9, baudrate: int = 115200, open_distance: float = 50.0, debug_communication: bool = False):
        self.connection = minimalmodbus.Instrument(com_port, address, debug=debug_communication)
        self.connection.serial.baudrate = baudrate
        self.open_distance = open_distance

        try:
            self.read_register(self.STATUS_REGISTER)
        except RobotiqCommunicationReadTimeoutError:
            raise RobotiqDeviceConnectionError(f'Cannot connect to Robotiq Hand-E gripper on com port: "{com_port}", address: {address}')

    #
    # Utility methods
    #

    def read_register(self, register: int, timeout: float = 2.0):
        start_time = time.time()

        while time.time() - start_time < timeout:
            try:
                return self.connection.read_register(register)
            except minimalmodbus.NoResponseError:
                time.sleep(0.5)

        raise RobotiqCommunicationReadTimeoutError(f'Timeout while reading register {register}')

    def read_registers(self, register: int, number: int, timeout: float = 2.0):
        start_time = time.time()

        while time.time() - start_time < timeout:
            try:
                return self.connection.read_registers(register, number)
            except minimalmodbus.NoResponseError:
                time.sleep(0.5)

        raise RobotiqCommunicationReadTimeoutError(f'Timeout while reading register {register}')

    def read_register_byte(self, register: int, byte_number: int, timeout: float = 2.0):
        data = self.read_register(register, timeout=timeout)

        if byte_number == 0:
            return data >> 8  # shift the left-most byte right 8 bits
        elif byte_number == 1:
            return data & 0xFF  # clear left-most 8 bits
        else:
            raise RobotiqError(f'Invalid byte_number in read_register_bytes(): "{byte_number}", must be 0 or 1')

    def write_registers(self, register: int, data: List[int], timeout: float = 2.0):
        start_time = time.time()

        while time.time() - start_time < timeout:
            try:
                return self.connection.write_registers(register, data)
            except minimalmodbus.NoResponseError:
                time.sleep(0.5)

        raise RobotiqCommunicationWriteTimeoutError(f'Timeout while writing register {register}, data: {data}')

    #
    # Input / Status registers
    #

    @property
    def status(self):
        return self.read_register_byte(self.STATUS_REGISTER, 0)

    @property
    def activated(self):
        return self.status & 0b00000001 > 0  # see if the first bit of the status byte is a 1

    @property
    def goto_status(self):
        return self.status & 0b00001000 > 0

    @property
    def automatic_release_status(self):
        return self.status & 0b00010000 > 0

    @property
    def automatic_release_direction_status(self):
        if self.status & 0b00010000 > 0:
            return self.AUTORELEASE_OPENING
        else:
            return self.AUTORELEASE_CLOSING

    @property
    def fault_status(self):
        return self.read_register_byte(self.FAULT_AND_POSITION_REGISTER, 0)

    @property
    def position_request_echo(self):
        return self.read_register_byte(self.FAULT_AND_POSITION_REGISTER, 1)

    @property
    def position(self):
        return self.read_register_byte(self.POSITION_AND_CURRENT_REGISTER, 0)

    @property
    def current(self):
        return self.read_register_byte(self.POSITION_AND_CURRENT_REGISTER, 1)

    #
    # Output / functionality registers
    #

    def activate(self, skip_if_activated = True):
        if skip_if_activated and self.activated:
            return

        # clear activation bit
        self.write_registers(self.ACTION_REGISTER, [0, 0, 0])
        # set activation bit
        self.write_registers(self.ACTION_REGISTER, [0x0100, 0x00FF, 0])

        status = self.read_register(self.STATUS_REGISTER)
        while status == 0x1100:
            status = self.read_register(self.STATUS_REGISTER)
            time.sleep(0.1)

        if status != 0x3100:
            raise RobotiqError(f'Invalid status during activation: {bin(status)}')

    def move_gripper(self, position: float, speed: float = 1.0, force: float = 1.0):
        self.activate()

        position_int = round((1 - position / self.open_distance) * 255)
        speed_int = round(speed * 255)
        force_int = round(force * 255)

        # start moving gripper
        self.write_registers(self.ACTION_REGISTER, [0x0900, position_int & 0x00FF, speed_int << 8 | force_int])

        status, fault_and_position_request, position_and_current = self.read_registers(self.STATUS_REGISTER, 3)
        # mask out object detection status and the "No communication during at least 1 second" fault
        while status == 0x3900 and fault_and_position_request & 0b011011111111 == 0x00FF:
            status, fault_and_position_request, position_and_current = self.connection.read_registers(2000, 3)
            time.sleep(0.1)

        # mask out the object detection status, since we don't care about it right now
        if status & 0b0011111100000000 != 0x3900 or fault_and_position_request & 0b011000000000 != 0:
            raise RobotiqError(f'Invalid status during movement: {bin(status)}, fault: {hex(status >> 8)}')


class RobotiqError(Exception):
    pass


class RobotiqCommunicationError(RobotiqError):
    pass


class RobotiqDeviceConnectionError(RobotiqError):
    pass


class RobotiqCommunicationReadTimeoutError(RobotiqCommunicationError):
    pass


class RobotiqCommunicationWriteTimeoutError(RobotiqCommunicationError):
    pass
